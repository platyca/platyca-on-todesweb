# Platyca on Todesweb

## Depends on:

* https://gitlab.com/todeslang/todeslang

## Install

```
cd .../src
sudo make install
```

## Run

In `screen` command:

```
todesweb platyca
```

```
cd .../caddy
sudo caddy run
```
